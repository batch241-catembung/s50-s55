import Banner from '../components/Banner';

export default function Error() {
    const data = {
        title: "Error 404 - Page Not Found",
        content: "The page you are looking for cannot be found.",
        destination: "/",
        label: "Back to Home"
    }
    return (
        <Banner data={data}/>
    )
}

// import {Button, Row, Col, Form} from 'react-bootstrap';
// import {useNavigate} from 'react-router-dom';

// export default function Error() {

//     const navigate =useNavigate(); 

//         function goingToHome(e){

//         e.preventDefault();
//         navigate('/'); 
        
//     };

// return (
//     <Form onSubmit={(e)=> goingToHome(e)}>
//         <Row>
//             <Col className="p-5">
//                 <h1>Page Not Found</h1>
//                 <p>go back to home page</p>
//                 <Button variant="primary" type="submit">click me</Button>
//             </Col>
//         </Row>
//     </Form>
//     )
// }
//         function goingToBanner(e){

//         e.preventDefault();
//         navigate('/Banner'); 
        
//     };

// return (
//     <Form onSubmit={(e)=> goingToBanner(e)}>
//         <Row>
//         	<Col className="p-5">
//                 <h1>Page Not Found</h1>
//                 <p>go back to home page</p>
//                 <Button variant="primary" type="submit">click me</Button>
//             </Col>
//         </Row>
//     </Form>
// 	)
// }
