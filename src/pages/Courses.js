import {useState, useEffect} from 'react';
//import courseData from '../data/coursesData'
import CourseCard from '../components/CourseCard';

export default function Courses(){

	//state that will be used to store the courses retrieved from the database
	const [courses, setCourses] = useState([]);

	// retrieves the courses from the database upon initial render opf the "Courses component"

	useEffect(()=>{
		fetch(`${process.env.REACT_APP_API_URL}/courses/active`)
		.then(res=> res.json())
		.then(data =>{
			console.log(data);
			setCourses(data.map(course=>{
				return(
			<CourseCard key={course.id} course = {course}/>
					)
			}))
		})
		

	},[])

	// check if the mock data is captured
	// console.log(courseData);
	// console.log(courseData[0]);

	// const courses = courseData.map(course=>{
	// 	return (
	// 		// <CourseCard key={course.id} courseProp = {course}/>
	// 		<CourseCard key={course.id} course = {course}/>
	// 		)
	// })

	return(
		<>
		{/*props drilling - we can pass information from one component to anoteher using props*/}
		{/*{} - used in props to signify that we are providing information*/}
		{/*<CourseCard courseProp = {courseData[0]}/>*/}
		{courses}
		</>
	)
}