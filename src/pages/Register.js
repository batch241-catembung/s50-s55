import { useState, useEffect, useContext } from 'react';
import {useNavigate} from 'react-router-dom';
import UserContext from '../UserContext';

import { Form, Button } from 'react-bootstrap';

import {Navigate} from 'react-router-dom';

import Swal from 'sweetalert2';


export default function Register() {

    const {user} = useContext(UserContext);

    //const{firstName, lastName, mobileNo, password} = useParams();
    const navigate = useNavigate();

    // State hooks to store the values of the input fields
    const [firstName, setFirstName] = useState('');
    const [lastName, setLastName] = useState('');
    const [email, setEmail] = useState('');
    const [mobileNo, setMobileNo] = useState('');
    const [password1, setPassword1] = useState('');
    const [password2, setPassword2] = useState('');
    // State to determine whether submit button will be enabled or not
    const [isActive, setIsActive] = useState(false);


    // Function to simulate user registration
    function registerUser(e) {
        // Prevents page from reloading
        e.preventDefault();

        fetch(`${process.env.REACT_APP_API_URL}/users/checkEmail`, {
            method: "POST",
            headers: {
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({
                email: email    
            }),
        })
        .then(res=>res.json())
        .then(data=>{
            console.log(data);
            if(data===true){

                Swal.fire({
                  title: "Duplicate Email Found",
                  icon: "error",
                  text: "Please provide different email."
              })
                
            } else {

                fetch(`${process.env.REACT_APP_API_URL}/users/register`, {
                    method: "POST",
                    headers: {
                        'Content-Type': 'application/json',
                    },
                    body: JSON.stringify({
                        firstName: firstName,
                        lastName: lastName,
                        email: email,
                        mobileNo:mobileNo,
                        password:password1
                    })
                })
                .then(res=>res.json())
                .then(data=>{
                    console.log(data);
                    
                    if (data.error) {
                        Swal.fire({
                          title: "Registration Failed",
                          icon: "error",
                          text: data.error,
                      });
                    } else {
                        Swal.fire({
                          title: "Successfully registered",
                          icon: "success",
                          text: "You have successfully registered to Zuitt.",
                      });
                        navigate("/login");
                    }
                    

                })

            }

        })

        // Clear input fields
        //setEmail('');
        //setPassword1('');
        //setPassword2('');

        //alert('Thank you for registering!')
    }

    useEffect(() => {
        // Validation to enable submit button when all fields are populated and both passwords match
        if((email !== '' && password1 !== '' && password2 !== '') && (password1 === password2)){
            setIsActive(true);
        } else {
            setIsActive(false);
        }
    // Dependencies
    // No dependencies - effect function will run every time component renders
    // With dependency (empty array) - effect function will only run (one time) when the component renders
    // With dependencies - effect function will run anytime one of the values in the array of dependencies changes
    }, [email, password1, password2]);

    return (
        (user.id !== null) ?
        <Navigate to ="/courses" />
        :
        <Form onSubmit={(e) => registerUser(e)}>

            <Form.Group controlId="userFirstName">
                <Form.Label>First Name</Form.Label>
                <Form.Control 
                type="text" 
                placeholder="Enter first name"
                value={firstName}
                onChange={e => setFirstName(e.target.value)} 
                required
                />
            </Form.Group>

            <Form.Group controlId="userLastName">
                <Form.Label>Last Name</Form.Label>
                <Form.Control 
                type="text" 
                placeholder="Enter last name"
                value={lastName}
                onChange={e => setLastName(e.target.value)} 
                required
                />
            </Form.Group>

            <Form.Group controlId="userEmail">
                <Form.Label>Email address</Form.Label>
                <Form.Control 
                    type="email" 
                    placeholder="Enter email"
                    value={email}
                    onChange={e => setEmail(e.target.value)} 
                    required
                />
                <Form.Text className="text-muted">
                    We'll never share your email with anyone else.
                </Form.Text>
            </Form.Group>

            <Form.Group controlId="userMobileNumber">
                <Form.Label>Mobile Number</Form.Label>
                <Form.Control 
                type="text" 
                placeholder="Enter Mobile Number"
                value={mobileNo}
               onChange={e => setMobileNo(e.target.value.slice(0, 11))}
        maxLength={11} 
                required
                />
            </Form.Group>

            <Form.Group controlId="password1">
                <Form.Label>Password</Form.Label>
                <Form.Control 
                    type="password" 
                    placeholder="Password" 
                    value={password1}
                    onChange={e => setPassword1(e.target.value)}
                    required
                />
            </Form.Group>

            <Form.Group controlId="password2">
                <Form.Label>Verify Password</Form.Label>
                <Form.Control 
                    type="password" 
                    placeholder="Verify Password" 
                    value={password2}
                    onChange={e => setPassword2(e.target.value)}
                    required
                />

            </Form.Group>
            {/*conditionally render submit button based on "isActive" state*/}
            { isActive ?
            <Button variant="primary" type="submit" id="submitBtn">
                Submit
            </Button>
            :
            <Button variant="danger" type="submit" id="submitBtn">
                Submit
            </Button>
            }
            
        </Form>
    )
}

// import {useState, useEffect} from 'react';

// import { Form, Button } from 'react-bootstrap';


// export default function Register() {

//     //state hooks to store the values of the input fields 
//     const [email, setEmail] = useState('');
//     const [password1, setPassword1] = useState('');
//     const [password2, setPassword2] = useState('');

//     //state to determine wether submit button will be enabled or not 
//     const[isActive, setIsActive] = useState(false);


//     //function tos imulate user registration
//     function registerUser(e){
//         //prevent page from reloading
//         e.preventDefault();

//         //clear input fields
//         setEmail('');
//         setPassword1('');
//         setPassword2('');
  
//         alert('Thank you for registering');
//     };


//     useEffect(()=>{
//         //validation to enable submit button when all fields are populated and both password match
//         if ((email !=='' && password1 !=='' && password2 !=='')&& password1===password2){
//             setIsActive(true)
//         }else{
//             setIsActive(false)
//         }
//     }, [email, password1, password2]);

//     return (
//         <Form onSubmit={(e)=> registerUser(e)}>
//             <Form.Group controlId="userEmail">
//                 <Form.Label>Email address</Form.Label>
//                 <Form.Control 
// 	                type="email" 
// 	                placeholder="Enter email"
//                     value={email}
//                     /*e means event*/
//                     onChange={e => setEmail(e.target.value)} 
// 	                required
//                 />
//                 <Form.Text className="text-muted">
//                     We'll never share your email with anyone else.
//                 </Form.Text>
//             </Form.Group>

//             <Form.Group controlId="password1">
//                 <Form.Label>Password</Form.Label>
//                 <Form.Control 
// 	                type="password" 
// 	                placeholder="Password" 
//                     value ={password1}
//                     onChange = {e => setPassword1(e.target.value)}
// 	                required
//                 />
//             </Form.Group>

//             <Form.Group controlId="password2">
//                 <Form.Label>Verify Password</Form.Label>
//                 <Form.Control 
// 	                type="password" 
// 	                placeholder="Verify Password" 
//                     value ={password2}
//                     onChange = {e => setPassword2(e.target.value)}
// 	                required
//                 />
//             </Form.Group>
//             {/*conditionally render submit button based on "isActive" state*/}
//             {isActive ?
//             <Button variant="primary" type="submit" id="submitBtn">
//                 Submit
//             </Button>
//             :
//             <Button variant="danger" type="submit" id="submitBtn">
//                 Submit
//             </Button>
//             }

//         </Form>
//     )

// }