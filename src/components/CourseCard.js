import {useState, useEffect} from 'react';

//import {useState} from 'react';

import {Card, Button} from 'react-bootstrap';
import {Link} from 'react-router-dom';
//export default function CourseCard({courseProp}) {
export default function CourseCard({course}) {
	//const {name, description, price} = courseProp;
	const {name, description, price, _id} = course;


	//count -getter
	//setCount - setter
	//use the satae hoo for this component to be able to state
	//state are used to keep track of information related  to individual components
	/*
		syntax
		const [getter, setter] = useState(initialGetterValue)
	*/
	// const [count, setCount] = useState(0);
	// console.log(useState);

	// const [seats, setSeats] = useState(30);
	// let seat = 30;


	// 	if (count !== seat){
	// 		setCount(count + 1);
	// 		seat--;
			
	// 	}else{
	// 		alert("No more seat available");
	// 	}
	// }	
	//  const [isOpen , setIsOpen] =useState(true)
	// function newEnroll(){
	// 	setCount(count + 1)
	// 	setSeats(seats - 1)
	// if (count < 30){
	// 		setCount(count + 1);
	// 	}else{
	// 		alert("No more seat available");
	// 	}
	//}
	//useEffect() - will allow us to execute a function if the value of seate state changes 
	// useEffect(()=>{
	// 	if (seats === 0 ){
	// 		setIsOpen(false);
	// 		document.querySelector(`#btn-enroll-${id}`).setAttribute('disabled', true)
	// 	}
	// 	//will run anytime one of the values in the arrat of the dependencies changes
	// }, [seats])
	//activity seats
	// const [remainingSeat, seat] = useState('30');
	// console.log(useState);

	// function newSeats(){
	// 	if (remainingSeat >= 1){
	// 		seat(remainingSeat - 1);
	// 	}else{
	// 		alert("No more seat available");
	// 	}
	// }

	return (
	<Card>
	    <Card.Body>
	        <Card.Title>{name}</Card.Title>
	        <Card.Subtitle>Description:</Card.Subtitle>
	        <Card.Text>{description}</Card.Text>
	        <Card.Subtitle>Price:</Card.Subtitle>
	        <Card.Text>{price}</Card.Text>
	        {/*<Card.Text>Enrollees: {count}</Card.Text>*/}
	        {/*<Card.Text>Seat: {seats}</Card.Text>*/}
	        {/*<Button variant="primary">Enroll</Button>*/}
	        {/*<Button id={`btn-enroll-${id}`} className="bg-primary" onClick={newEnroll}>Enroll</Button>*/}

{/*	       	<Card.Text>Seats: {remainingSeat}</Card.Text>
	        <Button className="bg-primary" onClick={newSeats}>Seats</Button>*/}
	        <Button className="bg-primary" as={Link} to ={`/courses/${_id}`}>Details</Button>
	    </Card.Body>
	</Card>
	)
}